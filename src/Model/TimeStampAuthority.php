<?php

namespace Dracoder\TrustedTimestamping\Model;

use Dracoder\TrustedTimestamping\Service\Request\TSAService;
use Dracoder\TrustedTimestamping\Service\Request\TimestampAuthorityServiceInterface;

class TimeStampAuthority
{
    public const FREE_TSA = 'free_tsa';
    public const FIRMAPROFESIONAL = 'firmaprofesional';
    public const VIA_FIRMA = 'via_firma';
    private const AUTHORITIES = [self::FREE_TSA, self::FIRMAPROFESIONAL, self::VIA_FIRMA];

    private const TSR_URLS = [
        self::FREE_TSA => 'https://freetsa.org/tsr',
        self::FIRMAPROFESIONAL => 'http://servicios.firmaprofesional.com/tsa',
        self::VIA_FIRMA => 'https://tsa.viafirma.com/viafirma-tsa/tsa'
    ];

    private const CA_CERTIFICATE = [
        self::FREE_TSA => 'cacert.crt',
        self::FIRMAPROFESIONAL => 'cacert.crt',
        self::VIA_FIRMA => 'cacert.crt'
    ];

    private const TSR_CERTIFICATE = [
        self::FREE_TSA => 'tsa.crt',
        self::FIRMAPROFESIONAL => 'tsa.crt',
        self::VIA_FIRMA => 'tsa.crt'
    ];

    /**
     * @param string $authority
     * @param bool $verifySsl
     *
     * @return TimestampAuthorityServiceInterface
     */
    public function getTimestampAuthorityService(string $authority, bool $verifySsl = true): ?TimestampAuthorityServiceInterface
    {
        if (in_array($authority, self::AUTHORITIES, true)) {
            return new TSAService($verifySsl);
        }

        return null;
    }

    /**
     * @param string $timestampAuthority
     *
     * @return string|null
     */
    public function getTimestampAuthorityUrl(string $timestampAuthority): ?string
    {
        return self::TSR_URLS[$timestampAuthority] ?? null;
    }

    /**
     * @param string $timestampAuthority
     *
     * @return string
     */
    private function getAssetDir(string $timestampAuthority): ?string
    {
        if (in_array($timestampAuthority, self::AUTHORITIES)) {
            return  __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.$timestampAuthority.DIRECTORY_SEPARATOR;
        }

        return null;
    }

    /**
     * @param string $timestampAuthority
     *
     * @return string|null
     */
    public function getCaCertificate(string $timestampAuthority): ?string
    {
        $assetDir = $this->getAssetDir($timestampAuthority);
        if ($assetDir && isset(self::CA_CERTIFICATE[$timestampAuthority])) {
            return realpath($assetDir.self::CA_CERTIFICATE[$timestampAuthority]);
        }

        return null;
    }

    /**
     * @param string $timestampAuthority
     *
     * @return string|null
     */
    public function getTsrCertificate(string $timestampAuthority): ?string
    {
        $assetDir = $this->getAssetDir($timestampAuthority);
        if ($assetDir && isset(self::TSR_CERTIFICATE[$timestampAuthority])) {
            return realpath($assetDir.self::TSR_CERTIFICATE[$timestampAuthority]);
        }

        return null;
    }
}
