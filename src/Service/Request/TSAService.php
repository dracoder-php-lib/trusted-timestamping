<?php

namespace Dracoder\TrustedTimestamping\Service\Request;

use Dracoder\TrustedTimestamping\Model\TimeStampAuthority;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use RuntimeException;

class TSAService implements TimestampAuthorityServiceInterface
{
    private string $requestTsrUrl;
    private Client $guzzle;
    private string $caCertificate;
    private ?string $tsaCertificate;

    /**
     * FreeTSAService constructor.
     *
     * @param string $authority
     * @param bool $verifySsl
     */
    public function __construct(string $authority, bool $verifySsl = true)
    {
        $this->guzzle = new Client(
            [
                'verify' => $verifySsl
            ]
        );
        $resolver = new TimeStampAuthority();
        $tsrUrl = $resolver->getTimestampAuthorityUrl($authority);
        if (!$tsrUrl) {
            throw new RuntimeException('TSA provider not found');
        }
        $this->requestTsrUrl = $tsrUrl;
        $caCertificate = $resolver->getCaCertificate($authority);
        if (!$caCertificate) {
            throw new RuntimeException('CA certificate for the provider not found');
        }
        $this->caCertificate = $caCertificate;
        $tsrCertificate = $resolver->getTsrCertificate($authority);
        $this->tsaCertificate = $tsrCertificate;
    }

    /**
     * @param string $tsq
     * @param string $tsrDestination
     * @param string|null $user
     * @param string|null $pass
     *
     * @return string
     */
    public function getTsr(string $tsq, string $tsrDestination, ?string $user = null, ?string $pass = null): ?string
    {
        try {
            $options = [];
            if ($user) {
                $options['auth'] = [$user, $pass];
            }
            $options['headers'] = ['Content-Type' => 'application/timestamp-query'];
            $options['body'] = fopen($tsq, 'rb');
            $options['sink'] = $tsrDestination;
            $this->guzzle->post(
                $this->requestTsrUrl,
                $options
            );

            return $tsrDestination;
        } catch (GuzzleException $e) {
            echo $e->getMessage();
            return null;
        }
    }

    /**
     * @param string $tsq
     * @param string $tsr
     *
     * @return bool
     */
    public function verifyTsr(string $tsq, string $tsr): bool
    {
        $cmd = 'openssl ts -verify -in '.$tsr.' -queryfile '.$tsq.' -CAfile '.$this->caCertificate;
        if ($this->tsaCertificate) {
            $cmd .= ' -untrusted '.$this->tsaCertificate;
        }
        $output = [];
        exec($cmd." 2>&1", $output, $resultCode);
        if ($resultCode !== 0) {
            //throw new RuntimeException("OpenSSL does not seem to be installed: ".implode(", ", $output));
            throw new RuntimeException(implode(", ", $output));
        }
        if (stripos($output[0], "openssl:Error") !== false) {
            throw new RuntimeException(
                "There was an error with OpenSSL. Is version >= 0.99 installed?: ".implode(", ", $output)
            );
        }
        foreach ($output as $line) {
            if ($line === 'Verification: OK') {
                return true;
            }
        }

        return false;
    }
}
