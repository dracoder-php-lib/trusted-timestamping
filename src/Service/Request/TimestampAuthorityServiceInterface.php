<?php

namespace Dracoder\TrustedTimestamping\Service\Request;

interface TimestampAuthorityServiceInterface
{
    /**
     * @param string $tsq
     * @param string $tsrDestination
     * @param string|null $user
     * @param string|null $password
     *
     * @return string
     */
    public function getTsr(string $tsq, string $tsrDestination, ?string $user, ?string $password): ?string;

    /**
     * @param string $tsq
     * @param string $tsr
     *
     * @return bool
     */
    public function verifyTsr(string $tsq, string $tsr): bool;
}
