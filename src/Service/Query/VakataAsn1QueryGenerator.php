<?php

namespace Dracoder\TrustedTimestamping\Service\Query;

use vakata\asn1\ASN1Exception;
use vakata\asn1\structures\TimestampRequest;

class VakataAsn1QueryGenerator extends AbstractTimestampQueryGenerator
{
    /**
     * @param string $hash
     * @param string $tsqDestination
     *
     * @return string
     */
    public function hashTsq(string $hash, string $tsqDestination): ?string
    {
        try {
            $tsq = TimestampRequest::generateFromHash($hash, true, false, 'sha512');
            if ($tsq) {
                file_put_contents($tsqDestination, $tsq);

                return $tsqDestination;
            }
        } catch (ASN1Exception $e) {
        }

        return null;
    }

    /**
     * @param string $data
     *
     * @return string
     */
    protected function getHash(string $data): string
    {
        return hash('sha512', $data, true);
    }
}
