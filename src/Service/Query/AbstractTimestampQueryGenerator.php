<?php

namespace Dracoder\TrustedTimestamping\Service\Query;

abstract class AbstractTimestampQueryGenerator
{
    /**
     * @param string $filename
     * @param string $tsqDestination
     *
     * @return string
     */
    public function fileTsq(string $filename, string $tsqDestination): ?string
    {
        return $this->stringTsq(file_get_contents($filename), $tsqDestination);
    }

    /**
     * @param string $data
     * @param string $tsqDestination
     *
     * @return string
     */
    public function stringTsq(string $data, string $tsqDestination): ?string
    {
        return $this->hashTsq($this->getHash($data), $tsqDestination);
    }

    /**
     * @param string $data
     *
     * @return string
     */
    abstract protected function getHash(string $data): string;

    /**
     * @param string $hash
     * @param string $tsqDestination
     *
     * @return string|null
     */
    abstract protected function hashTsq(string $hash, string $tsqDestination): ?string;
}
