<?php

namespace Dracoder\TrustedTimestamping\Service\Query;

use RuntimeException;

class OpenSSLQueryGenerator extends AbstractTimestampQueryGenerator
{
    /**
     * @param string $hash
     * @param $tsqDestination
     *
     * @return string|null
     */
    public function hashTsq(string $hash, $tsqDestination): ?string
    {
        if (strlen($hash) !== 128) { //
            throw new RuntimeException("Invalid hash. A binary sha512 hash should have 128 chars hash provided has ". strlen($hash));
        }

        $cmd = 'openssl ts -query -digest '.escapeshellarg($hash).' -cert -out '.escapeshellarg($tsqDestination). ' -sha512';
        $output = [];
        exec($cmd." 2>&1", $output, $resultCode);

        if ($resultCode !== 0) {
            throw new RuntimeException("OpenSSL does not seem to be installed: ".implode(", ", $output));
        }

        if (stripos($output[0], "openssl:Error") !== false) {
            throw new RuntimeException(
                "There was an error with OpenSSL. Is version >= 0.99 installed?: ".implode(", ", $output)
            );
        }

        return $tsqDestination;
    }

    /**
     * @param string $data
     *
     * @return string
     */
    protected function getHash(string $data): string
    {
        return hash('sha512', $data);
    }
}
