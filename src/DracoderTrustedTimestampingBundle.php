<?php

namespace Dracoder\TrustedTimestamping;

use Dracoder\TrustedTimestamping\DependencyInjection\DracoderTrustedTimestampingExtension;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DracoderTrustedTimestampingBundle extends Bundle
{
    public function getContainerExtension(): ExtensionInterface
    {
        return new DracoderTrustedTimestampingExtension();
    }
}
