<?php

namespace Dracoder\TrustedTimestamping\Test\Request;

use Dracoder\TrustedTimestamping\Model\TimeStampAuthority;
use Dracoder\TrustedTimestamping\Service\Request\TSAService;
use Dracoder\TrustedTimestamping\Test\AbstractTrustedTimestampingTestCase;

class FirmaprofesionalTest extends AbstractTrustedTimestampingTestCase
{
    public function testGetTsrFromVakataTsq(): void
    {
        if (file_exists(self::VAKATA_FIRMAPROFESIONAL_TSR_FILE)) {
            unlink(self::VAKATA_FIRMAPROFESIONAL_TSR_FILE);
        }

        $tsaService = new TSAService(TimeStampAuthority::FIRMAPROFESIONAL, false);
        $tsrFile = $tsaService->getTsr(
            self::VAKATA_TSQ_FILE,
            self::VAKATA_FIRMAPROFESIONAL_TSR_FILE,
            $_ENV['tsa_firmaprofesional_user'],
            $_ENV['tsa_firmaprofesional_pass']
        );

        self::assertNotNull($tsrFile);
        self::assertFileExists(self::VAKATA_FIRMAPROFESIONAL_TSR_FILE);
    }

    public function testGetTsrFromOpenSSLTsq(): void
    {
        if (file_exists(self::OPEN_SSL_FIRMAPROFESIONAL_TSR_FILE)) {
            unlink(self::OPEN_SSL_FIRMAPROFESIONAL_TSR_FILE);
        }

        $tsaService = new TSAService(TimeStampAuthority::FIRMAPROFESIONAL, false);
        $tsrFile = $tsaService->getTsr(
            self::OPEN_SSL_TSQ_FILE,
            self::OPEN_SSL_FIRMAPROFESIONAL_TSR_FILE,
            $_ENV['tsa_firmaprofesional_user'],
            $_ENV['tsa_firmaprofesional_pass']
        );

        self::assertNotNull($tsrFile);
        self::assertFileExists(self::OPEN_SSL_FIRMAPROFESIONAL_TSR_FILE);
    }

    public function testVerifyOpenSSLTSQ(): void
    {
        $verifier = new TSAService(TimeStampAuthority::FIRMAPROFESIONAL);
        $response = $verifier->verifyTsr(
            self::OPEN_SSL_TSQ_FILE,
            self::OPEN_SSL_FIRMAPROFESIONAL_TSR_FILE,
        );

        self::assertTrue($response);
    }

    public function testVerifyVakataTSQ(): void
    {
        $verifier = new TSAService(TimeStampAuthority::FIRMAPROFESIONAL);
        $response = $verifier->verifyTsr(
            self::VAKATA_TSQ_FILE,
            self::VAKATA_FIRMAPROFESIONAL_TSR_FILE,
        );

        self::assertTrue($response);
    }
}
