<?php

namespace Dracoder\TrustedTimestamping\Test;

use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

abstract class AbstractTrustedTimestampingTestCase extends TestCase
{
    //folders
    protected const EXAMPLES_FOLDER = __DIR__.DIRECTORY_SEPARATOR.'examples'.DIRECTORY_SEPARATOR;
    protected const EXAMPLE_TSR_FOLDER = self::EXAMPLES_FOLDER.'tsr'.DIRECTORY_SEPARATOR;
    protected const EXAMPLE_TSQ_FOLDER = self::EXAMPLES_FOLDER.'tsq'.DIRECTORY_SEPARATOR;
    //files
    protected const EXAMPLE_FILE = self::EXAMPLES_FOLDER.'example';
    protected const OPEN_SSL_TSQ_FILE = self::EXAMPLE_TSQ_FOLDER.'open_ssl.tsq';
    protected const OPEN_SSL_FREE_TSA_TSR_FILE = self::EXAMPLE_TSR_FOLDER.'open_ssl_free_tsa.tsr';
    protected const OPEN_SSL_FIRMAPROFESIONAL_TSR_FILE = self::EXAMPLE_TSR_FOLDER.'open_ssl_firmaprofesional.tsr';
    protected const OPEN_SSL_VIA_FIRMA_TSR_FILE = self::EXAMPLE_TSR_FOLDER.'open_ssl_via_firma.tsr';
    protected const VAKATA_TSQ_FILE = self::EXAMPLE_TSQ_FOLDER.'vakata.tsq';
    protected const VAKATA_FREE_TSA_TSR_FILE = self::EXAMPLE_TSR_FOLDER.'vakata_free_tsa.tsr';
    protected const VAKATA_FIRMAPROFESIONAL_TSR_FILE = self::EXAMPLE_TSR_FOLDER.'vakata_firmaprofesional.tsr';
    protected const VAKATA_VIA_FIRMA_TSR_FILE = self::EXAMPLE_TSR_FOLDER.'vakata_via_firma.tsr';

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $dotenv = Dotenv::createImmutable(__DIR__);
        $dotenv->load();
    }
}
