<?php

namespace Dracoder\TrustedTimestamping\Test\Query;

use Dracoder\TrustedTimestamping\Service\Query\OpenSSLQueryGenerator;
use Dracoder\TrustedTimestamping\Test\AbstractTrustedTimestampingTestCase;

class OpenSSLQueryGeneratorTest extends AbstractTrustedTimestampingTestCase
{
    public function testGetTsqFromFile(): void
    {
        if (file_exists(self::OPEN_SSL_TSQ_FILE)) {
            unlink(self::OPEN_SSL_TSQ_FILE);
        }

        $generator = new OpenSSLQueryGenerator();
        $tsqFile = $generator->fileTsq(self::EXAMPLE_FILE, self::OPEN_SSL_TSQ_FILE);

        self::assertNotNull($tsqFile);
        self::assertFileExists(self::OPEN_SSL_TSQ_FILE);
    }
}
