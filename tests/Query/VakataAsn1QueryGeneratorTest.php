<?php

namespace Dracoder\TrustedTimestamping\Test\Query;

use Dracoder\TrustedTimestamping\Service\Query\VakataAsn1QueryGenerator;
use Dracoder\TrustedTimestamping\Test\AbstractTrustedTimestampingTestCase;

class VakataAsn1QueryGeneratorTest extends AbstractTrustedTimestampingTestCase
{
    public function testGetTsqFromFile(): void
    {
        if (file_exists(self::VAKATA_TSQ_FILE)) {
            unlink(self::VAKATA_TSQ_FILE);
        }

        $generator = new VakataAsn1QueryGenerator();
        $tsqFile = $generator->fileTsq(self::EXAMPLE_FILE, self::VAKATA_TSQ_FILE);

        self::assertNotNull($tsqFile);
        self::assertFileExists(self::VAKATA_TSQ_FILE);
    }
}
